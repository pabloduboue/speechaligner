sub extract_words_one_line {
    my($s,$l)=@_;
    $_ = $l;
    chomp;
    s/^\"//;
    s/\"//;
    return $s if($_ eq chr(hex("e2")));
    return $s if($_ eq chr(hex("80")));
    if($_ eq chr(hex("99"))){
	$s.= "' ";
    }elsif($_ eq chr(hex("9c"))){
	$s.= "\" "; # open quote
    }elsif($_ eq chr(hex("9d"))){
	$s.= "\" "; # close quote
    }elsif($_ eq chr(hex("94"))){
	$s.= " "; # underscore, ignore
    }else{
	$s.= "$_ ";
    }
    $s;
}

sub extract_words_post_process{
    my($s)=@_;

    open(DICT,"/usr/share/dict/american-english-insane")||die"error reading dictionary: $!";
    my%dict=map {chomp; $_ => 1} <DICT>;
    close DICT;

    my$pos=0;
    $s=~tr/A-Z/a-z/;
    while(($pos = index($s," ' s ",$pos))>0){
	# print "$pos\n";
	# see if the words to the left are spelled out
	my$count = 0;
	my@letters=();
	while(substr($s, $pos-($count+1)*2,3)=~m/ [A-Za-z] /){
	    push@letters,substr($s, $pos-($count+1)*2+1,1);
	    $count++;
	}
	my$word;
	if($count>3){
	    # check for spurious 'a' (e.g., a p e r f o r m e r)
	    $word=join("",reverse(@letters));
	    if($dict{$word}){
		# we are game
	    }else{
		$count--;
		$word=substr($word,1);
	    }
	    # replace
	    $s = substr($s,0,$pos-$count*2)." $word ".substr($s,$pos+1);
	    $pos -= $count;
	}
	$pos++;
    }
    $s;
}

1;

