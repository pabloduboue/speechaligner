#!/usr/bin/perl -w

use strict;
use File::Temp qw( tempfile );

my$DIR=".";

require("$DIR/words-lib.pl");

my($fh2,$filename2)=tempfile( "tempXXXX", SUFFIX => '.txt' );
while(<STDIN>){
    print $fh2 $_;
}
close $fh2;

open(SCM,"$DIR/token_to_words.scm")||die"can't find scm file: $!";
my@scm=map {s/\"final.txt\"/\"$filename2\"/; $_ } <SCM>;
close SCM;
my($fh,$filename) = tempfile( "tempXXXX" , SUFFIX => '.scm' );
print $fh join("", @scm);
close $fh;


my@words = `/usr/bin/festival --heap 10000000 --batch $filename`;
unlink($filename);
unlink($filename2);
 
my$s="";
foreach my$w(@words){
    $s = &extract_words_one_line($s, $w);
}

$s = &extract_words_post_process($s);

print "$s\n";




