(require 'fileio)

(defUttType Text
  (Initialize utt)
  (Text utt)
  (Token_POS utt)
  (Token utt)
  (POS utt)
  (Phrasify utt)
  (Word utt))


(define (utt.baresynth utt)
    "(utt.baresynth UTT)
     Like utt.synth but without any after_synth_hooks"

    (apply_hooks before_synth_hooks utt)

    (let ((type (utt.type utt)))
      (let ((definition (assoc type UttTypes)))
	(if (null? definition)
	    (error "Unknown utterance type" type)
	    (let ((body (eval (cons 'lambda 
				    (cons '(utt) (cdr definition))))))
	      (body utt)))))
    utt)

(set! utt1 (utt.baresynth (eval (list 'Utterance 
		       'Text (read-file "final.txt")))))


(define (toend item) 
  (if item
      (begin
       (print (item.name item))
       (toend (item.next item)))))

(set! seg1 (utt.relation.first utt1 'Word))

(toend seg1)