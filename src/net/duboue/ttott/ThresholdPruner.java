package net.duboue.ttott;

import edu.cmu.sphinx.decoder.pruner.Pruner;
import edu.cmu.sphinx.decoder.search.ActiveList;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Double;

public class ThresholdPruner implements Pruner {

	private String name;

	@S4Double(defaultValue = 0.0)
	public final static String PROP_THRESHOLD = "threshold";

	private double threshold;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.cmu.sphinx.util.props.Configurable#newProperties(edu.cmu.sphinx.util
	 * .props.PropertySheet)
	 */
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		this.threshold = ps.getDouble(PROP_THRESHOLD);
	}

	public ThresholdPruner() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cmu.sphinx.util.props.Configurable#getName()
	 */
	public String getName() {
		return name;
	}

	/** Starts the pruner */
	@Override
	public void startRecognition() {
	}

	/**
	 * prunes the given set of states
	 * 
	 * @param activeList
	 *            a activeList of tokens
	 */
	@Override
	public ActiveList prune(ActiveList activeList) {
//		System.out.println(activeList.getBestScore());
		if (activeList.getBestScore() < threshold)
			return activeList.newInstance();
		return activeList.purge();
	}

	/** Performs post-recognition cleanup. */
	@Override
	public void stopRecognition() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cmu.sphinx.decoder.pruner.Pruner#allocate()
	 */
	@Override
	public void allocate() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cmu.sphinx.decoder.pruner.Pruner#deallocate()
	 */
	@Override
	public void deallocate() {

	}

}
