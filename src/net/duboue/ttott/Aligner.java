package net.duboue.ttott;

/*
 * Copyright 2010 Pablo Duboue.
 * Copyright 1999-2004 Carnegie Mellon University.
 * Portions Copyright 2004 Sun Microsystems, Inc.
 * Portions Copyright 2004 Mitsubishi Electric Research Laboratories.
 * All Rights Reserved.  Use is subject to license terms.
 *
 * See the file "license.terms" for information on usage and
 * redistribution of this file, and for a DISCLAIMER OF ALL
 * WARRANTIES.
 *
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import edu.cmu.sphinx.frontend.util.AudioFileDataSource;
import edu.cmu.sphinx.linguist.language.grammar.TextAlignerGrammar;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;

/**
 * A simple example that shows how to transcribe a continuous audio file that
 * has multiple utterances in it.
 */
public class Aligner {

	private static int DEFAULT_EXTERNAL_BUFFER_SIZE = 128000;

	public static void main(String[] args) throws IOException,
			UnsupportedAudioFileException {
		URL audioURL = null;
		File referenceFile = null;

		// Class cl = javazoom.spi.mpeg.sampled.file.MpegAudioFileReader.class;
		// Class cl = MpegAudioFileReader.class;

		// System.out.println(java.util.Arrays.asList(AudioSystem
		// .getAudioFileTypes()));

		if (args.length > 1) {
			audioURL = new File(args[0]).toURI().toURL();
			referenceFile = new File(args[1]);
		} else {
			System.err.println("Missing: audio file and text.");
			System.exit(-1);
		}
		StringBuilder sb = new StringBuilder();
		{
			BufferedReader br = new BufferedReader(
					new FileReader(referenceFile));
			String line = br.readLine();
			boolean started = false;
			while (line != null) {
				// line has already been token->words using Festival
				sb.append(line).append(" ");
				/*
				 * if (started && line.indexOf("CHAPTER II") >= 0) { break; } if
				 * (!started) { if (line.indexOf("CHAPTER I") >= 0) started =
				 * true; } else
				 * sb.append(line.toLowerCase().replaceAll("[^a-z0-9]", " "))
				 * .append('\n');
				 */
				line = br.readLine();
			}
			br.close();
		}

		URL configURL = Aligner.class.getResource("config.xml");

		ConfigurationManager cm = new ConfigurationManager(configURL);

		Recognizer recognizer = (Recognizer) cm.lookup("recognizer-align");

		TextAlignerGrammar grammar = (TextAlignerGrammar) cm
				.lookup("textAlignGrammar");
		grammar.setText(sb.toString());
		recognizer.addResultListener(grammar);

		/* allocate the resource necessary for the recognizer */
		recognizer.allocate();

		// configure the audio input for the recognizer
		AudioFileDataSource dataSource = (AudioFileDataSource) cm
				.lookup("audioFileDataSource");
		dataSource.setAudioFile(audioURL, null);
		sb.setLength(0);
		Result result = null;

		while ((result = recognizer.recognize()) != null) {

			String resultText = result.getTimedBestResult(true, true);
			sb.append(resultText).append(" ");
			System.out.println(resultText);
		}
		System.exit(0);

		// parse output
		String[] toks = sb.toString().split("\\s+");
		List<Object[]> wordAndTimings = new ArrayList<Object[]>(toks.length);
		for (String tok : toks) {
			if (tok.indexOf('(') < 0)
				continue;
			String[] ss = tok.split("\\(");
			String w = ss[0];
			ss = ss[1].split(",");
			wordAndTimings.add(new Object[] { w, new Double(ss[0]),
					new Double(ss[1].substring(0, ss[1].length() - 1)) });
		}

		AudioInputStream audioInputStream = AudioSystem
				.getAudioInputStream(audioURL);

		/*
		 * From the AudioInputStream, i.e. from the sound file, we fetch
		 * information about the format of the audio data. These information
		 * include the sampling frequency, the number of channels and the size
		 * of the samples. These information are needed to ask Java Sound for a
		 * suitable output line for this audio stream.
		 */
		AudioFormat audioFormat = audioInputStream.getFormat();
		int nSampleSizeInBits = 16;
		String strMixerName = null;
		int nExternalBufferSize = DEFAULT_EXTERNAL_BUFFER_SIZE;
		int nInternalBufferSize = AudioSystem.NOT_SPECIFIED;
		boolean bForceConversion = false;
		boolean bBigEndian = false;

		DataLine.Info info = new DataLine.Info(SourceDataLine.class,
				audioFormat, nInternalBufferSize);
		boolean bIsSupportedDirectly = AudioSystem.isLineSupported(info);
		if (!bIsSupportedDirectly || bForceConversion) {
			AudioFormat sourceFormat = audioFormat;
			AudioFormat targetFormat = new AudioFormat(
					AudioFormat.Encoding.PCM_SIGNED, sourceFormat
							.getSampleRate(), nSampleSizeInBits, sourceFormat
							.getChannels(), sourceFormat.getChannels()
							* (nSampleSizeInBits / 8), sourceFormat
							.getSampleRate(), bBigEndian);
			audioInputStream = AudioSystem.getAudioInputStream(targetFormat,
					audioInputStream);
			audioFormat = audioInputStream.getFormat();
		}

		SourceDataLine line = getSourceDataLine(strMixerName, audioFormat,
				nInternalBufferSize);
		if (line == null) {
			System.out
					.println("AudioPlayer: cannot get SourceDataLine for format "
							+ audioFormat);
			System.exit(1);
		}

		/*
		 * Still not enough. The line now can receive data, but will not pass
		 * them on to the audio output device (which means to your sound card).
		 * This has to be activated.
		 */
		line.start();

		/*
		 * Ok, finally the line is prepared. Now comes the real job: we have to
		 * write data to the line. We do this in a loop. First, we read data
		 * from the AudioInputStream to a buffer. Then, we write from this
		 * buffer to the Line. This is done until the end of the file is
		 * reached, which is detected by a return value of -1 from the read
		 * method of the AudioInputStream.
		 */
		int nBytesRead = 0;
		byte[] abData = new byte[nExternalBufferSize];
		long ms = 0;
		for (Object[] wAndT : wordAndTimings) {
			System.out.println(wAndT[0]);
			long start = (long) (((Double) wAndT[1]) * 1000);
			long end = (long) (((Double) wAndT[2]) * 1000);

			// how many frames to skip
			int skip = (int) (start - ms); // ms
			int skipFrames = (int) (audioFormat.getSampleRate() / 1000.0 * skip);
			int skipBytes = audioFormat.getFrameSize() * skipFrames;

			while (nBytesRead != -1 && skipBytes > 0) {
				try {
					nBytesRead = audioInputStream.read(abData, 0, Math.min(
							abData.length, skipBytes));
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (nBytesRead >= 0) {
					skipBytes -= nBytesRead;
					ms += (nBytesRead / audioFormat.getFrameSize())
							/ audioFormat.getFrameRate() * 1000.0;
					// int nBytesWritten = line.write(abData, 0, nBytesRead);
				}
			}

			// how many frames to play
			int play = (int) (end - ms); // ms
			int playFrames = (int) (audioFormat.getSampleRate() / 1000.0 * play);
			int playBytes = audioFormat.getFrameSize() * playFrames;

			while (nBytesRead != -1 && playBytes > 0) {
				try {
					nBytesRead = audioInputStream.read(abData, 0, Math.min(
							abData.length, playBytes));
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (nBytesRead >= 0) {
					playBytes -= nBytesRead;
					ms += (nBytesRead / audioFormat.getFrameSize())
							/ audioFormat.getFrameRate() * 1000.0;
					int nBytesWritten = line.write(abData, 0, nBytesRead);
				}
			}
			line.drain();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		line.close();
	}

	private static SourceDataLine getSourceDataLine(String strMixerName,
			AudioFormat audioFormat, int nBufferSize) {
		/*
		 * Asking for a line is a rather tricky thing. We have to construct an
		 * Info object that specifies the desired properties for the line.
		 * First, we have to say which kind of line we want. The possibilities
		 * are: SourceDataLine (for playback), Clip (for repeated playback) and
		 * TargetDataLine (for recording). Here, we want to do normal playback,
		 * so we ask for a SourceDataLine. Then, we have to pass an AudioFormat
		 * object, so that the Line knows which format the data passed to it
		 * will have. Furthermore, we can give Java Sound a hint about how big
		 * the internal buffer for the line should be. This isn't used here,
		 * signaling that we don't care about the exact size. Java Sound will
		 * use some default value for the buffer size.
		 */
		SourceDataLine line = null;
		DataLine.Info info = new DataLine.Info(SourceDataLine.class,
				audioFormat, nBufferSize);
		try {
			if (strMixerName != null) {
				Mixer.Info mixerInfo = AudioCommon.getMixerInfo(strMixerName);
				if (mixerInfo == null) {
					System.out.println("AudioPlayer: mixer not found: "
							+ strMixerName);
					System.exit(1);
				}
				Mixer mixer = AudioSystem.getMixer(mixerInfo);
				line = (SourceDataLine) mixer.getLine(info);
			} else {
				line = (SourceDataLine) AudioSystem.getLine(info);
			}

			/*
			 * The line is there, but it is not yet ready to receive audio data.
			 * We have to open the line.
			 */
			line.open(audioFormat, nBufferSize);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return line;
	}

	/**
	 * Common methods for audio examples.
	 */
	public static class AudioCommon {
		private static boolean DEBUG = false;

		public static void setDebug(boolean bDebug) {
			DEBUG = bDebug;
		}

		/**
		 * TODO:
		 */
		public static void listSupportedTargetTypes() {
			String strMessage = "Supported target types:";
			AudioFileFormat.Type[] aTypes = AudioSystem.getAudioFileTypes();
			for (int i = 0; i < aTypes.length; i++) {
				strMessage += " " + aTypes[i].getExtension();
			}
			out(strMessage);
		}

		/**
		 * Trying to get an audio file type for the passed extension. This works
		 * by examining all available file types. For each type, if the
		 * extension this type promisses to handle matches the extension we are
		 * trying to find a type for, this type is returned. If no appropriate
		 * type is found, null is returned.
		 */
		public static AudioFileFormat.Type findTargetType(String strExtension) {
			AudioFileFormat.Type[] aTypes = AudioSystem.getAudioFileTypes();
			for (int i = 0; i < aTypes.length; i++) {
				if (aTypes[i].getExtension().equals(strExtension)) {
					return aTypes[i];
				}
			}
			return null;
		}

		/**
		 * TODO:
		 */
		public static void listMixersAndExit() {
			out("Available Mixers:");
			Mixer.Info[] aInfos = AudioSystem.getMixerInfo();
			for (int i = 0; i < aInfos.length; i++) {
				out(aInfos[i].getName());
			}
			if (aInfos.length == 0) {
				out("[No mixers available]");
			}
			System.exit(0);
		}

		/**
		 * List Mixers. Only Mixers that support either TargetDataLines or
		 * SourceDataLines are listed, depending on the value of bPlayback.
		 */
		public static void listMixersAndExit(boolean bPlayback) {
			out("Available Mixers:");
			Mixer.Info[] aInfos = AudioSystem.getMixerInfo();
			for (int i = 0; i < aInfos.length; i++) {
				Mixer mixer = AudioSystem.getMixer(aInfos[i]);
				Line.Info lineInfo = new Line.Info(
						bPlayback ? SourceDataLine.class : TargetDataLine.class);
				if (mixer.isLineSupported(lineInfo)) {
					out(aInfos[i].getName());
				}
			}
			if (aInfos.length == 0) {
				out("[No mixers available]");
			}
			System.exit(0);
		}

		/**
		 * TODO: This method tries to return a Mixer.Info whose name matches the
		 * passed name. If no matching Mixer.Info is found, null is returned.
		 */
		public static Mixer.Info getMixerInfo(String strMixerName) {
			Mixer.Info[] aInfos = AudioSystem.getMixerInfo();
			for (int i = 0; i < aInfos.length; i++) {
				if (aInfos[i].getName().equals(strMixerName)) {
					return aInfos[i];
				}
			}
			return null;
		}

		/**
		 * TODO:
		 */
		public static TargetDataLine getTargetDataLine(String strMixerName,
				AudioFormat audioFormat, int nBufferSize) {
			/*
			 * Asking for a line is a rather tricky thing. We have to construct
			 * an Info object that specifies the desired properties for the
			 * line. First, we have to say which kind of line we want. The
			 * possibilities are: SourceDataLine (for playback), Clip (for
			 * repeated playback) and TargetDataLine (for recording). Here, we
			 * want to do normal capture, so we ask for a TargetDataLine. Then,
			 * we have to pass an AudioFormat object, so that the Line knows
			 * which format the data passed to it will have. Furthermore, we can
			 * give Java Sound a hint about how big the internal buffer for the
			 * line should be. This isn't used here, signaling that we don't
			 * care about the exact size. Java Sound will use some default value
			 * for the buffer size.
			 */
			TargetDataLine targetDataLine = null;
			DataLine.Info info = new DataLine.Info(TargetDataLine.class,
					audioFormat, nBufferSize);
			try {
				if (strMixerName != null) {
					Mixer.Info mixerInfo = getMixerInfo(strMixerName);
					if (mixerInfo == null) {
						out("AudioCommon.getTargetDataLine(): mixer not found: "
								+ strMixerName);
						return null;
					}
					Mixer mixer = AudioSystem.getMixer(mixerInfo);
					targetDataLine = (TargetDataLine) mixer.getLine(info);
				} else {
					if (DEBUG) {
						out("AudioCommon.getTargetDataLine(): using default mixer");
					}
					targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
				}

				/*
				 * The line is there, but it is not yet ready to receive audio
				 * data. We have to open the line.
				 */
				if (DEBUG) {
					out("AudioCommon.getTargetDataLine(): opening line...");
				}
				targetDataLine.open(audioFormat, nBufferSize);
				if (DEBUG) {
					out("AudioCommon.getTargetDataLine(): opened line");
				}
			} catch (LineUnavailableException e) {
				if (DEBUG) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				if (DEBUG) {
					e.printStackTrace();
				}
			}
			if (DEBUG) {
				out("AudioCommon.getTargetDataLine(): returning line: "
						+ targetDataLine);
			}
			return targetDataLine;
		}

		/**
		 * Checks if the encoding is PCM.
		 */
		public static boolean isPcm(AudioFormat.Encoding encoding) {
			return encoding.equals(AudioFormat.Encoding.PCM_SIGNED)
					|| encoding.equals(AudioFormat.Encoding.PCM_UNSIGNED);
		}

		/**
		 * TODO:
		 */
		private static void out(String strMessage) {
			System.out.println(strMessage);
		}

	}

}
